<?php

class Vente {

    private $id;
    private $date;
    private $remise;
    private $fraisliv;
    private $statut;
    private $numrueliv;
    private $nomrueliv;
    private $compadliv;
    private $villeliv;
    private $cpliv;
    private $numruefac;
    private $nomruefac;
    private $compadfac;
    private $villefac;
    private $cpfac;
    private $total;
    
    public function __construct(array $vente) {
        if ($_GET['act'] != 'create' ){
            $this->setId($vente['id']);
        }
        $this->setDate($vente['date']);
        $this->setRemise($vente['remise']);
        $this->setFraisliv($vente['fraisliv']);
        $this->setStatut($vente['statut']);
        $this->setNumrueliv($vente['numrueliv']);
        $this->setNomrueliv($vente['nomrueliv']);
        $this->setCompadliv($vente['compadliv']);
        $this->setVilleliv($vente['villeliv']);
        $this->setCpliv($vente['cpliv']);
        $this->setNumruefac($vente['numruefac']);
        $this->setNomruefac($vente['nomruefac']);
        $this->setCompadfac($vente['compadfac']);
        $this->setVillefac($vente['villefac']);
        $this->setCpfac($vente['cpfac']);
        $this->setTotal($vente['total']);
    }

    public function getId() {
        return $this->id;
    }

    public function getDate() {
        return $this->date;
    }
    
    public function getRemise() {
        return $this->remise;
    }

    public function getFraisliv() {
        return $this->fraisliv;
    }

    public function getStatut() {
        return $this->statut;
    }

    public function getNumrueliv() {
        return $this->numrueliv;
    }

    public function getNomrueliv() {
        return $this->nomrueliv;
    }

    public function getCompadliv() {
        return $this->compadliv;
    }

    public function getVilleliv() {
        return $this->villeliv;
    }

    public function getCpliv() {
        return $this->cpliv;
    }
    public function getNumruefac() {
        return $this->numruefac;
    }

    public function getNomruefac() {
        return $this->nomruefac;
    }

    public function getCompadfac() {
        return $this->compadfac;
    }

    public function getVillefac() {
        return $this->villefac;
    }

    public function getCpfac() {
        return $this->cpfac;
    }
    private function setId($id) {
        if (is_int($id) && $id >= 0) {
            $this->id = $id;
        } //else gérer exception
    }

    public function setDate($date) {
        if (is_date($date) && !empty($date)) {
            $this->date = $date;
        } //else gérer exception
    }
    
    public function setRemise($remise) {
        if (is_numeric($remise)) {
            $this->remise = $remise;
        } //else gérer exception
    }

    public function setFraisliv($fraisliv) {
        if (is_bool($fraisliv)) {
            $this->fraisliv = $fraisliv;
        } //else gérer exception
    }

    public function setStatut($statut) {
        if (is_string($statut) && !empty($statut)) {
            $this->statut = $statut;
        } //else gérer exception
    }

    public function setNumrueliv($numrueliv) {
        if (is_int($numrueliv) && !empty($numrueliv)) {
            $this->numrueliv = $numrueliv;
        } //else gérer exception
    }

    public function setNomrueliv($nomrueliv) {
        if (is_string($nomrueliv) && !empty($nomrueliv)) {
            $this->nomrueliv = $nomrueliv;
        } //else gérer exception
    }
    
    public function setCompadliv($compadliv) {
        if (is_string($compadliv)) {
            $this->compadliv = $compadliv;
        } //else gérer exception
    }

    public function setVilleliv($villeliv) {
        if (is_string($villeliv) && !empty($villeliv)) {
            $this->villeliv = $villeliv;
        } //else gérer exception
    }

    public function setCpliv($cpliv) {
        if (is_int($cpliv) && !empty($cpliv)) {
            $this->cpliv = $cpliv;
        } //else gérer exception
    }
    public function setNumruefac($numruefac) {
        if (is_int($numruefac) && !empty($numruefac)) {
            $this->numruefac = $numruefac;
        } //else gérer exception
    }

    public function setNomruefac($nomruefac) {
        if (is_string($nomruefac) && !empty($nomruefac)) {
            $this->nomruefac = $nomruefac;
        } //else gérer exception
    }
    
    public function setCompadfac($compadfac) {
        if (is_string($compadfac)) {
            $this->compadfac = $compadfac;
        } //else gérer exception
    }

    public function setVillefac($villefac) {
        if (is_string($villefac) && !empty($villefac)) {
            $this->villefac = $villefac;
        } //else gérer exception
    }

    public function setCpfac($cpfac) {
        if (is_int($cpfac) && !empty($cpfac)) {
            $this->cpfac = $cpfac;
        } //else gérer exception
    }

    public function setTotal($total) {
        if (is_numeric($total) && !empty($total)) {
            $this->total = $total;
        } //else gérer exception
    }

}