<?php

class ClientManager {

    private $db;

    public function __construct() {
        $this->db = DataBase::getInstance();
    }

    public function create(Client $client) {

        $req = $this->db->prepare('
            INSERT INTO T_CLIENT (clinom, cliprenom, clicivilite, climailpro, climailperso)
            VALUES (:nom, :prenom, :civilite, :mailpro, :mailperso)
		    ');
        $req->execute([
            'nom'=> $client->getNom(),
            'prenom'=> $client->getPrenom(),
            'civilite'=> $client->getCivilite(),
            'mailpro'=> $client->getMailpro(),
            'mailperso'=> $client->getMailperso()
            ]);
        $req = $this->db->query('
            SELECT id FROM T_CLIENT WHERE 
            ');
    }
    
    public function read($id_client = NULL) {
        $req = $this->db->prepare('
            SELECT * FROM T_CLIENT
            WHERE id_cli = :id
        ');
        $req->execute([
            'id' => $id_client
        ]);
        $result = $req->fetch(PDO::FETCH_ASSOC);
        return new Client(array(
            'id' => $result['ID_CLI'],
            'nom' => $result['CLINOM'],
            'prenom' => $result['CLIPRENOM'],
            'civilite' => $result['CLICIVILITE'],
            'mailpro'   => $result['CLIMAILPRO'],
            'mailperso'   => $result['CLIMAILPERSO']
        ));
    }

    public function update(Client $client) {
        $req = $this->db->prepare('
            UPDATE T_CLIENT
            SET clinom=:nom, cliprenom=:prenom, clicivilite=:civilite, climailpro=:mailpro, climailperso=:mailperso
            WHERE id_cli=:id
        ');
        $req->execute([
            'id'=> $client->getId(),
            'nom'=> $client->getNom(),
            'prenom'=> $client->getPrenom(),
            'civilite'=> $client->getCivilite(),
            'mailpro'=> $client->getMailpro(),
            'mailperso'=> $client->getMailperso()
        ]);
    }

    public function delete($id) {
        $req = $this->db->prepare('
            DELETE FROM T_CLIENT
            WHERE id_cli=:id
        ');

        $req->execute([
            'id'=> $id
            ]);
    }

    public function all() {
        $req = $this->db->query('
            SELECT id_cli FROM T_CLIENT
        ');
        $id_client = $req->fetchAll( PDO::FETCH_COLUMN);
        if (!empty($id_client)) {
            foreach ($id_client as $id) {
                $listClient[] = $this->read($id);
            }
            return $listClient;
        }
    }

    public function setEntreprise(Client $client, Entreprise $entrep) {
        $req = $db->prepare('
            UPDATE T_CLIENT
            SET id_entrep=:id_entrep
            WHERE id_cli=:id_cli
            ');

        $req->execute([
            'id_cli'=> $client->getId(),
            'id_entrep'=> $entrep->getId()
            ]);
    }

    public function count() {
        return $db->query('
            SELECT COUNT(*) FROM T_CLIENT 
	        ');
    }

}