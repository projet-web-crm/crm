<?php

class Client {

    private $id;
    private $nom;
    private $prenom;
    private $civilite;
    private $mailpro;
    private $mailperso;

    const CIVILITE_HOMME = "Mr";
    const    CIVILITE_FEMME = "Mme";

    
    public function __construct(array $client) {
        if (isset($_GET['id']) || $_GET['act'] == 'all' || $_GET['act'] == 'update') {
            $this->setId($client['id']);
        }
        $this->setNom($client['nom']);
        $this->setPrenom($client['prenom']);
        $this->setCivilite($client['civilite']);
        $this->setMailpro($client['mailpro']);
        $this->setMailperso($client['mailperso']);
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }
    
    public function getPrenom() {
        return $this->prenom;
    }

    public function getCivilite() {
        return $this->civilite;
    }

    public function getMailpro() {
        return $this->mailpro;
    }

    public function getMailperso() {
        return $this->mailperso;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        if (is_string($nom) && !empty($nom)) {
            $this->nom = $nom;
        } //else gérer exception
    }
    
    public function setPrenom($prenom) {
        if (is_string($prenom) && !empty($prenom)) {
            $this->prenom = $prenom;
        } //else gérer exception
    }

    public function setCivilite($civilite) {
        if (($_GET['act'] == 'create') || ($_GET['act'] == 'update')) {
            $this->civilite = ($civilite == 0) ?FALSE :TRUE;
        } else {
            switch ($civilite) {
                case 0 :
                case FALSE :
                    $this->civilite = self::CIVILITE_FEMME;
                    break;
                case 1 :
                case TRUE :
                    $this->civilite = self::CIVILITE_HOMME;
                    break;
                default:
            }
        } //else gérer exception
    }

    public function setMailpro($mailpro) {
        if (is_string($mailpro) && !empty($mailpro)) {
            $this->mailpro = $mailpro;
        } //else gérer exception
    }

    public function setMailperso($mailperso) {
        if (is_string($mailperso) && !empty($mailperso)) {
            $this->mailperso = $mailperso;
        } //else gérer exception
    }
}