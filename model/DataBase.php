<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:57
 */

class DataBase {

    private $_db;

    const DB_TYPE = 'mysql:host=localhost;';
    const DB_NAME = 'dbname=CRM;';
    const DB_ENCODAGE = 'charset=utf8';
    const DB_PSEUDO = 'user';
    const DB_PASSWORD = 'resu';

    private static $instance;

    private function __construct() {
        try {
            $this->_db = new PDO(self::DB_TYPE.self::DB_NAME.self::DB_ENCODAGE,
                self::DB_PSEUDO,
                self::DB_PASSWORD
            );
        } catch (PDOException $e) {
            echo $e->getMessage()." ". $e->getLine();
        }
    }

    private function getDb() {
        return $this->_db;
    }

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new DataBase();
        }

        return self::$instance->getDb();
    }


}