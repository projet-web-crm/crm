<?php


class Produit {

	private $id;	
	private $nom;
	private $desi;
	private $prix; 
	private $tva; 

	public function __construct(array $produit){
		if ($_GET['act'] != 'create') {
			$this->setId($produit['id']);
		}
		$this->setNom($produit['nom']);
		$this->setDesi($produit['desi']);
		$this->setPrix($produit['prix']);
		$this->setTva($produit['tva']);
	}

	public function getId() {
		return $this->id;
	}

	public function getNom() {
		return $this->nom;
	}

	public function getDesi() {
		return $this->desi;
	}

	public function getPrix() {
		return $this->prix;
	}	

	public function getTva() {
		return $this->tva;
	}	

	private function setId($id) {
		$id = intval($id);
		if (is_int($id) && $id >= 0) {
			$this->id = $id;
		} 
	}
	// Condition format de l'ID

	public function setNom($nom) {
		if (is_string($nom) && !empty($nom)) {
			$this->nom = $nom;
		}
	}

	public function setDesi($desi) {
		if (is_string($desi)) {
			$this->desi = $desi;
		}
	}

	public function setPrix($prix) {
		if (is_numeric($prix) && $prix >= 0) {
			$this->prix = $prix;
		}
	}

	public function setTva($tva) {
		if (is_numeric($tva) && $tva >= 0) {
			$this->tva = $tva;
		}
	}
}