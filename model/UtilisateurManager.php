<?php

class UtilisateurManager {

    private $db;

    public function __construct() {
        $this->db = DataBase::getInstance();
    }

    public function create(Utilisateur $utilisateur) {

        $req = $this->db->prepare('
            INSERT INTO T_UTILISATEUR (utilnom, utilprenom, utilcivilite, utilmail, utilmdp)
            VALUES (:nom, :prenom, :civilite, :mail, :mdp)
		    ');
        $req->execute([
            'nom'=> $utilisateur->getNom(),
            'prenom'=> $utilisateur->getPrenom(),
            'civilite'=> $utilisateur->getCivilite(),
            'mail'=> $utilisateur->getMail(),
            'mdp' => $utilisateur->getMdp()
            ]);
    }
    
    public function read($id) {
        if ($id) {
            $req = $this->db->prepare('
                SELECT * FROM T_UTILISATEUR
                WHERE id_util = :id
            ');
            $req->execute([
                'id' => $id
            ]);
            $result = $req->fetch(PDO::FETCH_ASSOC);
            return new Utilisateur(array(
                'id' => $result['ID_UTIL'],
                'nom' => $result['UTILNOM'],
                'prenom' => $result['UTILPRENOM'],
                'civilite' => $result['UTILCIVILITE'],
                'mail' => $result['UTILMAIL'],
                'mdp' =>$result['UTILMDP']
            ));
        }
    }

    public function update(Utilisateur $utilisateur) {
        $req = $db->prepare('
            UPDATE T_UTILISATEUR
            SET utilnom=:nom, utilprenom=:prenom, utilcivilite=:civilite, utilmail=:mail, utilmdp=:mdp
            WHERE id_util=:id
        ');

        $req->execute([
            'id'=> $utilisateur->getId(),
            'nom'=> $utilisateur->getNom(),
            'prenom'=> $utilisateur->getPrenom(),
            'civilite'=> $utilisateur->getCivilite(),
            'mail'=> $utilisateur->getMail(),
            'mdp'=> $utilisateur->getMdp()
        ]);
    }

    public function delete(Utilisateur $utilisateur) {
        $req = $this->db->prepare('
            DELETE FROM T_UTILISATEUR
            WHERE id_util=:id
        ');

        $req->execute([
            'id'=> $utilisateur->getId()
            ]);
    }

    public function all() {
        $req = $this->db->query('
            SELECT id_util FROM T_UTILISATEUR
        ');
        $id_utilisateur = $req->fetchAll( PDO::FETCH_COLUMN);
        foreach ($id_utilisateur as $id) {
            $listUtilisateur[] = $this->read($id);
        }
        return $listUtilisateur;
    }

    public function count() {
        return $db->query('
            SELECT COUNT(*) FROM T_UTILISATEUR 
	        ');
    }

}