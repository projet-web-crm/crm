<?php
require Database.php;

class AdresseManager {

	private $db;

	public function __construct() {
		$this->db = new Database;
	}

	public function create(Adresse $adresse) {
		$req = $db->prepare('
		INSERT INTO T_ADRESSE (adlabel, adnumrue, adnomrue, adcptad, adville, adcp)
		VALUES (:label, :numrue, :nomrue, :cptad, :ville, :cp)
		');

		$req->execute([
			'label'=> $adresse->getLabel(),
			'numrue'=> $numrue->getNumrue(),
			'nomrue'=> $nomrue->getNomrue(),
			'cptad'=> $cptad->getCptad(),
			'ville'=> $ville->getVille(),
			'cp'=> $cp->getCp(),
		]);
	}

	public function read(Adresse $adresse) {
		$req = $db->prepare('
			SELECT * FROM T_ADRESSE
			WHERE id_ad=:id
		');

		$req->execute([
			'id'=>$adresse->getId()
		]);
	}

	public function update(Adresse $adresse) {
		$req = $db->prepare('
			UPDATE T_ADRESSE
			SET adlabel=:label, adnumrue=:numrue, adnomrue=:nomrue, adcptad=:cptad, adville=:ville, adcp=:cp
			WHERE id_ad=:id
		');
	}

	public function delete(Adresse $adresse) {
		$req = $db->prepare('
			DELETE FROM T_ADRESSE
			WHERE id_ad=:ad
		');

		$req->execute([
			'id'=> $adresse->getId()
		]);
	}
}