<?php


class Tva {

	private $id;	
	private $valeur;

	public function __construct(array $tva){
		if ($_GET['act'] != 'create') {
			$this->setId($tva['id']);
		}
		$this->setValeur($tva['valeur']);
	}	
	public function getId() {
		return $this->id;
	}

	public function getValeur() {
		return $this->valeur;
	}

	private function setId($id) {
		$id = intval($id);
		if (is_int($id) && $id >= 0) {
			$this->id = $id;
		}
	}

	public function setValeur($valeur) {
		if (is_numeric($valeur) && $valeur >= 0) {
			$this->valeur = $valeur;
		}
	}
}