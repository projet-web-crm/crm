<?php

class TvaManager {

	private $db;

	public function __construct() {
		$this->db = DataBase::getInstance();
	}

	public function create (Tva $tva) {
		$req = $this->db->prepare('
			INSERT INTO T_TVA (tvavaleur)
			VALUES (:valeur)
		');

		$req->execute([
			'valeur'=> $tva->getValeur()
		]);
	}

	public function read($id) {
		$req = $this->db->prepare('
			SELECT * FROM T_TVA
			WHERE id_tva=:id
		');

		$req->execute([
    		'id'=>$id
		]);
		$result = $req->fetch(PDO::FETCH_ASSOC);
        return new Tva(array(
            'id' => $result['ID_TVA'],
            'valeur' => $result['TVAVALEUR']
		));
	}

	public function update(Tva $tva) {
		$req = $this->db->prepare ('
			UPDATE T_TVA
			SET tvavaleur=:valeur
			WHERE id_tva=:id
			');

		$req->execute([
			'id'=>$tva->getId(),
			'valeur'=>$tva->getValeur()
		]);
	}

	public function delete($id) {
		$req = $this->db->prepare('
			DELETE FROM T_TVA
			WHERE id_tva=:id
			');

		$req->execute([
			'id'=>$id
		]);
	}

	public function all() {
        $req = $this->db->query('
            SELECT id_tva FROM T_TVA
        ');
        $id_tva = $req->fetchAll( PDO::FETCH_COLUMN);
        foreach ($id_tva as $id) {
			$listTva[] = $this->read($id);
        }
        return $listTva;
    }
}