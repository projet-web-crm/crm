<?php

class Entreprise {

    private $id;
    private $nom;
    private $siret;
    
    public function __construct(array $entreprise) {
        if ($_GET['entreprise'] != 'create' ){
            $this->setId($entreprise['id_entrep']);
        }
        $this->setNom($entreprise['entrepnom']);
        $this->setSiret($entreprise['entrepsiret']);
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }
    
    public function getSiret() {
        return $this->siret;
    }

    private function setId($id) {
        if (is_int($id) && $id >= 0) {
            $this->id = $id;
        } //else gérer exception
    }

    public function setNom($nom) {
        if (is_string($nom) && !empty($nom)) {
            $this->nom = $nom;
        } //else gérer exception
    }
    
    public function setSiret($siret) {
        if (is_int($siret)) {
            $this->siret = $siret;
        } //else gérer exception
    }
}