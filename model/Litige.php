<?php


class Litige {

	private $id;	
	private $date;
	private $com;

	public function __construct(array $litige){
		if ($_GET['litige'] != 'create') {
			$this->setid($litige['id_lit']);
		}
		$this->setdate($litige['litdate']);
		$this->setcom($litige['litcom']);
	}

	public function getId() {
		return $this->id;
	}

	public function getDate() {
		return $this->date;
	}

	public function getCom() {
		return $this->com;
	}

	private function setId($id) {
		if (is_int($id)) && $id >= 0) {
			$this->id = $id;
		}
	}

	public function setDate($date) {
		if (is_date($date) && $date >= 0) {
			$this->date = $date;
		}
	}

	public function setCom($com) {
		if (is_string($com) && !empty($com)){
			$this->com = $nom;
		}

	}