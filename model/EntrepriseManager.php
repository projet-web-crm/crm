<?php
require DataBase.php;

class EntrepriseManager {

    private $db;

    public function __construct(DataBase $db) {
        $this->db = $db;
    }

    public function create(Entreprise $entreprise) {
        $req = $db->prepare('
            INSERT INTO T_ENTREPRISE (entrepnom, entrepsiret)
            VALUES (:nom, :siret)
		');

        $req->execute([
            'nom'=> $entreprise->getNom(),
            'siret'=> $entreprise->getSiret()
        ]);
    }
    
    public function read(Entreprise $entreprise) {
        $req = $db->prepare('
            SELECT * FROM T_ENTREPRISE
            WHERE id_entrep=:id
	    ');

        $req->execute([
            'id'=>$entreprise->getId()
        ]);
    }

    public function update(Entreprise $entreprise) {
        $req = $db->prepare('
            UPDATE T_ENTREPRISE
            SET entrepnom=:nom, entrepsiret=:siret
            WHERE id_entrep=:id
        ');

        $req->execute([
            'id'=>$entreprise->getId(),
            'nom'=> $entreprise->getNom(),
            'siret'=> $entreprise->getSiret()
        ]);
    }

    public function delete(Entreprise $entreprise) {
        $req = $db->prepare('
            DELETE FROM T_ENTREPRISE
            WHERE id_entrep=:id
        ');

        $req->execute([
            'id'=>$entreprise->getId()
        ]);
    }

}