<?php
require DataBase.php;

class TelephoneManager {

    private $db;

    public function __construct() {
        $this->db = new DataBase;
    }

    public function create(Telephone $telephone) {
    	$req = $db->prepare('
    		INSERT INTO T_TELEPHONE (tellabel, telnumero)
    		VALUES (:label, :numero)
    		');

    	$req = $db->execute([
    		'label'=> $telephone->getLabel()
    		'numero'=> $numero->getNumero()
    	]);
    }

    public function read(Telephone $telephone) {
    	$req = $db->prepare('
    		SELECT * FROM T_TELEPHONE
    		WHERE id_prod=:id
    		');

    	$req->execute([
    		'id'=>$telephone->getId()
    	]);
    }

    public function update(Telephone $telephone)
    	$req = $db->prepare('
    		UPDATE T_TELEPHONE
    		SET tellabel=:label, telnumero=:numero
    		WHERE id_tel=:id
    		')

    	$req = $db->execute([
    		'id'=> $telephone->getId(),
    		'label'=> $telephone->getLabel(),
    		'numero'=> $telephone->getNumero()
    	]);

    public function delete(Telephone $telephone) {
    	$req = $db->prepare('
    		DELETE FROM T_TELEPHONE
    		WHERE id_tel=:id
    		');

    	$req->execute([
    		'id'=> $telephone->getid()
    	]);
    }
}