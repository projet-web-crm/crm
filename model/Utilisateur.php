<?php

class Utilisateur {

    private $id;
    private $nom;
    private $prenom;
    private $civilite;
    private $mail;
    private $mdp;
    
    public function __construct(array $utilisateur) {
        $this->setId($utilisateur['id']);
        $this->setNom($utilisateur['nom']);
        $this->setPrenom($utilisateur['prenom']);
        $this->setCivilite($utilisateur['civilite']);
        $this->setMail($utilisateur['mail']);
        $this->setMdp($utilisateur['mdp']);
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }
    
    public function getPrenom() {
        return $this->prenom;
    }

    public function getCivilite() {
        return $this->civilite;
    }

    public function getMail() {
        return $this->mail;
    }

    public function getMdp() {
        return $this->mdp;
    }

    public function setId($id) {
        $id = intval($id);
        if (is_int($id) && $id >= 0) {
            $this->id = $id;
        } //else gérer exception
    }

    public function setNom($nom) {
        if (is_string($nom) && !empty($nom)) {
            $this->nom = $nom;
        } //else gérer exception
    }
    
    public function setPrenom($prenom) {
        if (is_string($prenom) && !empty($prenom)) {
            $this->prenom = $prenom;
        } //else gérer exception
    }

    public function setCivilite($civilite) {
        switch ($civilite) {
            case 0 :
                $this->civilite = 'Mme';
                break;
            case 1:
                $this->civilite = 'Mr';
                break;
            default:

        } //else gérer exception
    }

    public function setMail($mail) {
        if (is_string($mailpro) && !empty($mailpro)) {
            $this->mail = $mail;
        } //else gérer exception
    }

    public function setMdp($mdp) {
        if (is_string($id) && id >= 8) {
            $this->mdp = $mdp;
        }
    }
}