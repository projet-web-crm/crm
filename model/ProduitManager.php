<?php

class ProduitManager {

    private $db;

    public function __construct() {
        $this->db = DataBase::getInstance();
    }

    public function create(Produit $produit) {
    	$req = $this->db->prepare('
    		INSERT INTO T_PRODUIT (id_tva, prodnom, proddesi, prodprix)
    		VALUES (:tva, :nom, :desi, :prix)
		');
		
    	$req->execute([
    		'tva'=> $produit->getTva(),
    		'nom'=> $produit->getNom(),
    		'desi'=> $produit->getDesi(),
    		'prix'=> $produit->getPrix()
    	]);
    }

    public function read($id) {
    	$req = $this->db->prepare('
    		SELECT * FROM T_PRODUIT
    		WHERE id_prod=:id
    	');

    	$req->execute([
    		'id'=>$id
		]);
		$result = $req->fetch(PDO::FETCH_ASSOC);
        return new Produit(array(
            'id' => $result['ID_PROD'],
            'nom' => $result['PRODNOM'],
            'desi' => $result['PRODDESI'],
			'prix' => $result['PRODPRIX'],
			'tva' => $result['ID_TVA']
		));
    }

    public function update(Produit $produit) {
    	$req = $this->db->prepare('
    		UPDATE T_PRODUIT
    		SET prodnom=:nom, proddesi=:desi, prodprix=:prix, id_tva=:tva
    		WHERE id_prod=:id
    	');

    	$req->execute([
    		'id'=> $produit->getId(),
    		'nom'=> $produit->getNom(),
    		'desi'=> $produit->getDesi(),
			'prix'=> $produit->getPrix(),
			'tva' => $produit->getTva()
    	]);
	}

    public function delete($id) {
   		$req = $this->db->prepare('
   			DELETE FROM T_PRODUIT
   			WHERE id_prod=:id
   		');

   		$req->execute([
   			'id'=> $id
   		]);
	}
	   
	public function all() {
        $req = $this->db->query('
            SELECT id_prod FROM T_PRODUIT
        ');
        $id_produit = $req->fetchAll( PDO::FETCH_COLUMN);
        foreach ($id_produit as $id) {
			$listProduit[] = $this->read($id);
        }
        return $listProduit;
    }
}