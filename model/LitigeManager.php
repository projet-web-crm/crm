<?php
require DataBase.php;

class LitigeManager {

    private $db;

    public function __construct() {
        $this->db = new DataBase;
    }

    public function create(Litige $litige) {
    	$req = $db->prepare('
    		INSERT INTO T_LITIGE (litdate, litcom)
    		VALUES (:dat, :com)
    		');

    	$req->execute([
    		'dat'=> $litige->getDate(),
    		'com'=> $litige->getCom(),
    	]);
    }

    public function read(Litige $litige) {
    	$req = $db->prepare('
    		SELECT * FROM T_LITIGE
    		WHERE id_lit=:id
    		')

    	$req->execute([
    		'id'=>$litige->getId()
    	]);
    }

    public function update(Litige $litige){
    	$req = $db->prepare('
    		UPDATE T_LITIGE
    		SET litdate=:dat, litcom=:com
    		WHERE id_lit=:id
    		')

    	$req = $db->execute([
    		'id'=> $litige->getId()
    		'dat'=> $litige->getDate()
    		'com'=> $litige->getCom()
    	]);
    }