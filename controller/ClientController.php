<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 14:51
 */


class ClientController {

    public function all() {
        $manager = new ClientManager();
        $clients = $manager->all();
        require 'view/GestionClient/Client/listClientView.php';
    }

    public function create() {
        $manager = new ClientManager();
        $client = new Client(array(
            'nom' => $_POST['nom'],
            'prenom' => $_POST['prenom'],
            'civilite' => $_POST['civ'],
            'mailpro' => $_POST['mailpro'],
            'mailperso'   => $_POST['mailperso']
        ));
        if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['civ'])) {
            $manager->create($client);
            header('Location: ?page=client&act=all');
        } else {
            header('Location: ?page=client&act=form');
        }
    }

    public function read() {
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            header('Location: ?page=client&act=all');
        }
        $manager = new ClientManager();
        $client = $manager->read($_GET['id']);
        require 'view/GestionClient/Client/displayDetailClientView.php';
    }

    public function update() {
        $manager = new ClientManager();
        $client = new Client(array(
            'id' => $_GET['id'],
            'nom' => $_POST['nom'],
            'prenom' => $_POST['prenom'],
            'civilite' => $_POST['civ'],
            'mailpro' => $_POST['mailpro'],
            'mailperso'   => $_POST['mailperso']
        ));
        if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['civ'])) {
            $manager->update($client);
            header('Location: ?page=client&act=read&id='.$_GET['id']);
        } else {
            header('Location: ?page=client&act=form&id='.$_GET['id']);
        }
    }

    public function delete() {
        $manager = new ClientManager();
        $manager->delete($_GET['id']);
        header('Location: ?page=client&act=all');
    }

    public function form() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $id = NULL;
        }
        $manager = new ClientManager();
        $client = $manager->read($id);
        require 'view/GestionClient/Client/formClientView.php';
    }
}


