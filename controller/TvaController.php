<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 14:51
 */


class TvaController {

    public function all() {
        $manager = new TvaManager();
        $tvas = $manager->all();
        require 'view/GestionProduit/Tva/listTvaView.php';
    }

    public function create() {
        $manager = new TvaManager();
        $tva = new Tva(array(
            'valeur' => $_POST['valeur']
        ));
        $manager->create($tva);
        header('Location: ?page=tva&act=all');;
    }

    public function read() {
        $id = $_GET['id'];
        $manager = new TvaManager();
        $tva = $manager->read($id);
        require 'view/GestionProduit/Tva/listTvaView.php';
    }

    public function update() {
        $manager = new TvaManager();
        $tva = new Tva(array(
            'id' => $_GET['id'],
            'valeur' => $_POST['valeur']
        ));
        var_dump($tva);
        $tva = $manager->update($tva);
        header('Location: ?page=tva&act=all');
    }

    public function delete() {
        $id = $_GET['id'];
        $manager = new TvaManager();
        $tva = $manager->delete($id);
        header('Location: ?page=tva&act=all');
    }

    public function form() {
        require 'view/GestionProduit/Tva/formTvaView.php';
    }

}
