<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 14:51
 */


class ProduitController {

    public function all() {
        $manager = new ProduitManager();
        $produits = $manager->all();
        require 'view/GestionProduit/Produit/listProduitView.php';
    }

    public function create() {
        $manager = new ProduitManager();
        $produit = new Produit(array(
            'nom' => $_POST['nom'],
            'desi' => $_POST['desi'],
            'prix' => $_POST['prix'],
            'tva' => $_POST['tva']
        ));
        $manager->create($produit);
        header('Location: ?page=produit&act=all');;
    }

    public function read() {
        $id = $_GET['id'];
        $manager = new ProduitManager();
        $produit = $manager->read($id);

        $managerTva = new TvaManager();
        $tva = $managerTva->read($produit->getTva());
        require 'view/GestionProduit/Produit/displayProduitView.php';
    }

    public function update() {
        $manager = new ProduitManager();
        $produit = new Produit(array(
            'id' => $_GET['id'],
            'nom' => $_POST['nom'],
            'desi' => $_POST['desi'],
            'prix' => $_POST['prix'],
            'tva' => $_POST['tva']
        ));
        $produit = $manager->update($produit);
        header('Location: ?page=produit&act=all');
    }

    public function delete() {
        $id = $_GET['id'];
        $manager = new ProduitManager();
        $produit = $manager->delete($id);
        header('Location: ?page=produit&act=all');
    }

    public function form() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $id=null;
        }
        $manager = new ProduitManager();
        $produit = $manager->read($id);

        $managerTva = new TvaManager();
        $tvas = $managerTva->all();
        require 'view/GestionProduit/Produit/formProduitView.php';
    }

}
