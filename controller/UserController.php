<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 14:51
 */


class UserController {

    public function create() {
        $manager = new UtilisateurManager();
        $utilisateur = new Utilisateur(array(
            'nom' => $_POST['nom'],
            'prenom' => $_POST['prenom'],
            'civ' => $_POST['civ'],
            'mail' => $_POST['mail'],
            'mdp' => $_POST['mdp']
        ));
        $manager->create($utilisateur);
        // header('Location: ?page=produit&act=all');
        require 'view/GestionUtilisateur/displayUtilisateurView.php';
    }

    public function read() {
        $id = $_GET['id'];
        $manager = new UtilisateurManager();
        $produit = $manager->read($id);
        require 'view/GestionUtilisateur/displayUtilisateurView.php';
    }

    public function update() {
        /*$manager = new UtilisateurManager();
        $produit = new Utilisateur(array(
            'id' => $_GET['id'],
            'nom' => $_POST['nom'],
            'prenom' => $_POST['prenom'],
            'civ' => $_POST['civ'],
            'mail' => $_POST['mail'],
            'mdp' => $_POST['mdp']
            //tva
        ));
        $utilisateur = $manager->update($utilisateur);
        // header('Location: ?page=produit&act=all');*/
        require 'view/GestionUtilisateur/formUtilisateurView.php';
    }

    public function delete() {
        $id = $_GET['id'];
        $manager = new UtilisateurManager();
        $utilisateur = $manager->delete($id);
        // header('Location: ?page=produit&act=all');
        require 'view/GestionUtilisateur/formConnexionView.php';
    }

    public function form() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            $id=null;
        }
        $manager = new UtilisateurManager();
        $utilisateur = $manager->read($id);
        require 'view/GestionUtilisateur/formUtilisateurView.php';
    }

}
