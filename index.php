<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:43
 */

session_start();

spl_autoload_register(function($className) {
    if (file_exists('model/'.$className.'.php')) {
        require 'model/'.$className.'.php';
    } else {
        require 'controller/'.$className.'.php';
    }
});

$pages = [
    'client' => 'ClientController',
    'entreprise' => 'EntrepriseController',
    'produit' => 'ProduitController',
    'user' => 'UserController',
    'tva' => 'TvaController'
];

if (isset($_GET['page']) && isset($pages[$_GET['page']])) {
    $class = $pages[$_GET['page']];
    $object = new $class();

    if (method_exists($object, $_GET['act'])) {
        call_user_func_array(array($object, $_GET['act']), array());
    } else {
        echo "rediriger sur une page par default, mauvaise action";
    }
    return;
}else {
    require 'view/GestionUtilisateur/formConnexionView.php';
}




