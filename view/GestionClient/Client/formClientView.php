<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Création client"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">

	<h1> Nouveau client </h1>
</div>


<form method="post" action="?page=client&act=<?= (isset($_GET['id'])) ?'update&id='.$_GET['id'] :'create' ?>">
    <div class="form-group row">
        <div class="col-sm-2 col-form-label">Civilité</div>
        <div class="col-sm-4">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="civ" id="civhomme" value="1" checked>
                <label class="form-check-label" for="civhomme">
                    Homme
                </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="civ" id="civfemme" value="0">
                <label class="form-check-label" for="civfemme">
                    Femme
                </label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="nom" class="col-sm-2 col-form-label">Nom</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="nomclient" name="nom" placeholder="Entrer le nom du client..." value="<?= $client->getNom(); ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="prenom" class="col-sm-2 col-form-label">Prenom</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="prenomclient" name="prenom" placeholder="Entrer le prénom du client..." value="<?= $client->getPrenom(); ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="mailPro" class="col-sm-2 col-form-label">Mail Professionnel</label>
        <div class="col-sm-4">
            <input type="email" class="form-control" id="prenomclient" name="mailpro" placeholder="Entrer le mail professionnel du client..." value="<?= $client->getMailpro(); ?>">
        </div>
    </div>
    <div class="form-group row">
        <label for="mailPerso" class="col-sm-2 col-form-label">Mail Personnel</label>
        <div class="col-sm-4">
            <input type="email" class="form-control" id="prenomclient" name="mailperso" placeholder="Entrer le mail personnel du client..." value="<?= $client->getMailperso() ?>">
        </div>
    </div>
    <div class="form-group row">
        <a href="index.php" class="col-sm-2 col-form-label">Annuler</a>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Valider</button>
        </div>
    </div>
</form>
<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>