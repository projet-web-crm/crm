<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Actions client"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">

  <h1>Actions Client</h1>
  <h3 class="mt-4"> Nom client </h3>

</div>

<div> 


<div class="container">

	<select name id="listeactions">

	<option value="Devis"> Devis </option>
	<option value="Commande"> Commandes passées </option>
	<option value="Produits"> Produits achetés </option>
	<option value="Echtel"> Échanges téléphoniques </option>
	<option value="Litiges"> Litiges </option>
	
</select>
  <div class="row mt-2 ml-0">
	<div class="col-2 border">
           
            <p class="mt-4"> 28/11/2018 </p>
            <p class="mt-4"> 22/07/2018 </p>
            <p class="mt-4"> 02/02/2018 </p>
            <p class="mt-4"> 18/05/2017 </p>
            <p class="mt-4"> 24/01/2017 </p>
         </div>


        <div class="col-8 border">

          <p class="mt-4"> Commande xxx </p>
          <p class="mt-4"> Devis xxx </p>
          <p class="mt-4"> Commande xxx </p>
          <p class="mt-4"> Devis xxx </p>
          <p class="mt-4"> Echange téléphonique </p>


      </div>
  </div>
</div>
</div>
<div class="jumbotron">

  <h1>Infos Client</h1>
</div>


<div class="container">
  <div class="row">
    <div class="col-12 list-group-item">

      <p><?= $client->getCivilite().' '.$client->getNom().' '.$client->getPrenom() ?> </p>
      <p>Mail professionnel : <?= $client->getMailpro() ?> </p>
      <p>Mail personnel : <?= $client->getMailperso() ?> </p>



      <div class="">
      <a type="button" class="btn btn-primary" href="?page=client&act=form&id=<?= $client->getId() ?>" >Modifier</a>
      <a type="button" class="btn btn-primary" href="?page=client&act=delete&id=<?= $client->getId() ?>" >Supprimer</a>
      </div>

    </div>

    <div class="w-100 row justify-content-around">

        <a href="#" class="btn-success:hover my-3" role="button" aria-disabled="false">Saisir échange téléphonique</a>

        <a href="#" class="btn-success:hover my-3" ml-sm-3 role="button" aria-disabled="true">Saisir devis </a>

        <a href="#" class="btn-success:hover my-3" ml-sm-4 role="button" aria-disabled="true">Saisir commande </a>

    </div>

        <div class="col-2 list-group-item">

            <p class="mt-4"> 28/11/2018 </p>
            <p class="mt-4"> 22/07/2018 </p>
            <p class="mt-4"> 02/02/2018 </p>
            <p class="mt-4"> 18/05/2017 </p>
            <p class="mt-4"> 24/01/2017 </p>
         </div>


        <div class="col-10 list-group-item">

          <p class="mt-4"> Commande xxx </p>
          <p class="mt-4"> Devis xxx </p>
          <p class="mt-4"> Commande xxx </p>
          <p class="mt-4"> Devis xxx </p>
          <p class="mt-4"> Echange téléphonique </p>


        </div>
        <div class="">
            <button type="button" class="btn btn-primary">Détails</button>
        </div>

  </div>
</div>
<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>