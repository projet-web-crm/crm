<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 22:29
 */

$title = "Liste Client"; ?>

<?php $css = "public/Bootstrap/css/datatables.min.css" ?>

<?php ob_start(); ?>
<div class="jumbotron row justify-content-between">
    <h1>Liste des Clients</h1>
    <a class="btn btn-primary align-self-center" href="?page=client&act=form" role="button">Ajouter un nouveau client</a></td>
</div>
<table id="dtBasicExample" class="table table-striped table-bordered table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th id="th-sm">ID</th>
        <th class="th-sm">Nom</th>
        <th class="th-sm">Prénom</th>
    </tr>
    </thead>
    <tbody>
    <?php if(!empty($clients)) { foreach ($clients as $client) { ?>
        <tr>
            <td><?= $client->getId(); ?></td>
            <td><a href="?page=client&act=read&id=<?= $client->getId(); ?>"><?= $client->getNom(); ?></a></td>
            <td><a href="?page=client&act=read&id=<?= $client->getId(); ?>"><?= $client->getPrenom(); ?></a></td>
        </tr>
    <?php }}?>
    </tbody>
</table>
<script type="text/javascript" src="public/Bootstrap/js/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#dtBasicExample').DataTable({
            "searching": true // false to disable search (or any other option)
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>
<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>


