<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Téléphone"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">

  <h1>Ajout n° téléphone</h1>
</div>


<form>
  <div class="form-group row">
    <label for="Nom" class="col-sm-2 col-form-label">N° Téléphone</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="tel" placeholder="(Renseigner n° de téléphone...)">
    </div>
  </div>

<fieldset class="form-group">
    <div class="form-group row">
      <legend class="col-form-label col-sm-2 pt-0">Label</legend>
        <div class="col-sm-4">
          <div class="form-check">
            <input class="form-check-input" type="radio" name="tel" id="fixepers" value="Tel" checked>
            <label class="form-check-label" for="fixepers">
                Fixe (personnel)
            </label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="tel" id="fixepro" value="Tel">
              <label class="form-check-label" for="fixepro">
                Fixe (professionnel)
            </label>
          </div>
          <div class="form-check">  
            <input class="form-check-input" type="radio" name="tel" id="mobilpers" value="Tel" checked>
            <label class="form-check-label" for="mobilepers">
                Mobile (personnel)
            </label>
          </div>

          <div class="form-check">
            <input class="form-check-input" type="radio" name="tel" id="mobilpro" value="Tel">
              <label class="form-check-label" for="mobilpro">
                Mobile (professionnel)
            </label>
          </div>
        </div>
    </div>
    </fieldset>


  <div class="form-group row">
          <a href="index.php" class="col-sm-2 col-form-label">Annuler</a>
          <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Valider</button>
          </div>
  </div>

</form>



<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>