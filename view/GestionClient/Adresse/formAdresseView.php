<?php

 $title = "Adresse"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>

<div class="jumbotron"> 
    <h1>Adresse</h1>
</div>

<form>
    <div class="form-group row">
            <label for="label" class="col-sm-2 col-form-label">Nommer l'adresse</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="label" placeholder="Donner un nom à cette adresse">
        </div>
    </div>
    <div class="form-group row">
            <label for="numrue" class="col-sm-2 col-form-label">Numéro de rue</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="numrue" placeholder="Numéro de rue">
        </div>
    </div>
    <div class="form-group row">
            <label for="nomrue" class="col-sm-2 col-form-label">Nom de rue</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="nomrue" placeholder="Nom de rue">
        </div>
    </div>
    <div class="form-group row">
            <label for="cptad" class="col-sm-2 col-form-label">Complément d'adresse</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="cptad" placeholder="Complément d'adresse">
        </div>
    </div>
    <div class="form-group row">
            <label for="cp" class="col-sm-2 col-form-label">Code postal</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="cp" placeholder="Code postal">
        </div>
    </div>
    <div class="form-group row">
            <label for="ville" class="col-sm-2 col-form-label">Ville</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="ville" placeholder="Ville">
        </div>
    </div>
    <div class="form-group row">
        <a class="col-sm-2 col-form-label" href="index.php">Retour à l'accueil</a>
        <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Ajouter l'adresse</button>
        </div>
    </div>
</form>


<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>