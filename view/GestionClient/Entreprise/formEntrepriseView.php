<?php

 $title = "Entreprise"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>

<div class="jumbotron"> 
    <h1>Entreprise</h1>
</div>

<form>
    <div class="form-group row">
            <label for="nom" class="col-sm-2 col-form-label">Nom de l'entreprise</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="nom" placeholder="Saisir le nom de l'entreprise">
        </div>
    </div>
    <div class="form-group row">
            <label for="siret" class="col-sm-2 col-form-label">SIRET</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="siret" placeholder="Saisir le numéro de SIRET">
        </div>
    </div>

    <div class="form-group row">
        <a class="col-sm-2 col-form-label" href="index.php">Retour à l'accueil</a>
        <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Ajouter l'entreprise</button>
        </div>
    </div>
</form>


<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>