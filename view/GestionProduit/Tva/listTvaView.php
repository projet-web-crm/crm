<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 22:29
 */

$title = "TVA"; ?>

<?php $css = "public/Bootstrap/css/datatables.min.css" ?>

<?php ob_start(); ?>

<div class="jumbotron row justify-content-between">
	<h1>Liste des TVA enregistrées</h1>
    <a class="btn btn-primary align-self-center" href="?page=tva&act=form" role="button">Ajouter une nouvelle TVA</a>
</div>

<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm">Id</th>
        <th class="th-sm">Nom</th>
        <th>Modifier</th>
        <th>Supprimer</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($tvas as $tva) { ?>
        <tr>
            <form method="POST" action="?page=tva&act=update&id=<?= $tva->getId() ?>">
                <td><?=$tva->getId()?></td>
                <td><input type="number" class="form-control" id="nom" name="valeur" value="<?=$tva->getValeur()?>"></td>
                <td><button type="submit" class="btn btn-primary ">Modifier</button></td>
                <td><a class="btn btn-danger " href="?page=tva&act=delete&id=<?= $tva->getId() ?>" role="button">Supprimer</a></td>
            </form>
        </tr>
    <?php } ?>
    </tbody>
</table>
<script type="text/javascript" src="public/Bootstrap/js/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#dtBasicExample').DataTable({
            "searching": false // false to disable search (or any other option)
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>
<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>