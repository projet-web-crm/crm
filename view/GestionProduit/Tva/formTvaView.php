<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "TVA"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">
	<h1>Ajouter une TVA</h1>
</div>


<form method="POST" action="?page=tva&act=create">
  <div class="form-group row">
    <label for="valeur" class="col-sm-2 col-form-label">Valeur</label>
    <div class="col-sm-4">
      <input type="number" class="form-control" id="valeur" name="valeur" placeholder="Saisir une valeur de TVA">
    </div>
  </div>

	<div class="form-group row">
       <a href="?page=tva&act=all" class="col-sm-2 col-form-label">Annuler</a>
       <div class="col-sm-4">
           <button type="submit" class="btn btn-primary">Valider</button>
       </div>
   </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>