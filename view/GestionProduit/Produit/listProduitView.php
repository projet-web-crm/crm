<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 22:29
 */

$title = "Produit"; ?>

<?php $css = "public/Bootstrap/css/datatables.min.css" ?>

<?php ob_start(); ?>

<div class="jumbotron row justify-content-between">
	<h1>Liste des produits</h1>
    <a class="btn btn-primary align-self-center" href="?page=produit&act=form" role="button">Ajouter un nouveau produit</a>
</div>

<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="th-sm">Id</th>
        <th class="th-sm">Nom</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($produits as $produit) { ?>
        <tr>
            <td><?= $produit->getId() ?></td>
            <td><a href="?page=produit&act=read&id=<?= $produit->getId() ?>"><?= $produit->getNom() ?></a></td>
            
        </tr>
    <?php } ?>
    </tbody>
</table>
<script type="text/javascript" src="public/Bootstrap/js/datatables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#dtBasicExample').DataTable({
            "searching": true // false to disable search (or any other option)
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>
<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>