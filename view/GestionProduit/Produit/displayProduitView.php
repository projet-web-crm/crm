<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Produit"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron row justify-content-between">
	<h1>Consulter produit</h1>
    <div class="align-self-center">
        <a class="btn btn-primary " href="?page=produit&act=form&id=<?= $produit->getId() ?>" role="button">Modifier</a>
        <a class="btn btn-danger " href="?page=produit&act=delete&id=<?= $produit->getId() ?>" role="button">Supprimer</a>
    </div>
</div>

<div class="form-group row">
    <p class="col-sm-2 col-form-label">Id/Ref :</p>
    <div class="col-sm-4">
        <p><?=$produit->getId()?></p>
    </div>
</div>
<div class="form-group row">
    <p class="col-sm-2 col-form-label">Nom :</p>
    <div class="col-sm-4">
        <p><?=$produit->getNom()?></p>
    </div>
</div>
<div class="form-group row">
    <p class="col-sm-2 col-form-label">Description :</p>
    <div class="col-sm-4">
        <p><?=$produit->getDesi()?></p>
    </div>
</div>
<div class="form-group row">
    <p class="col-sm-2 col-form-label">Prix :</p>
    <div class="col-sm-4">
        <p><?=$produit->getPrix()?></p>
    </div>
</div>

<div class="form-group row">
    <p class="col-sm-2 col-form-label">TVA :</p>
    <div class="col-sm-4">
        <p><?=$tva->getValeur().'%'?></p>
    </div>
</div>  

<div class="form-group">
    <a href="?page=produit&act=all" class="col-sm-2 col-form-label">Retour à la liste des produits</a>
</div>


<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>