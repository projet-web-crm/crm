<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Produit"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">
	<h1><?=(isset($_GET['id']))? 'Modifier ' : 'Créer ' ?>produit</h1>
</div>


<form method="POST" action="?page=produit&act=<?= (isset($_GET['id']))? 'update&id='.$_GET['id'] : 'create' ?>">
  <div class="form-group row">
    <label for="nom" class="col-sm-2 col-form-label">Nom</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="nom" name="nom" placeholder="Saisir le nom du produit" value="<?=$produit->getNom()?>">
    </div>
  </div>
  <div class="form-group row">
    <label for="description" class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-4">
      <input type="text" class="form-control" id="description" name="desi" placeholder="Saisir la description" value="<?=$produit->getDesi()?>">
    </div>
  </div>
	<div class="form-group row">
		<label for="prix" class="col-sm-2 col-form-label">Prix</label>
		<div class="col-sm-4">
			<input type="text" class="form-control" id="prix" name="prix" placeholder="Saisir le prix" value="<?=$produit->getPrix()?>">
		</div>
	</div>
	<div class="form-group row">
		<label for="tva" class="col-sm-2 col-form-label">TVA</label>
		<div class="col-sm-4">
			<select name="tva" class="form-control" id="tva">
        <?php foreach ($tvas as $tva) { ?>
          <option value="<?= $tva->getId() ?>"><?= $tva->getValeur() ?></option>
        <?php } ?>
      </select>
		</div>
	</div>
	<div class="form-group row">
       <a href="?page=produit&act=all" class="col-sm-2 col-form-label">Annuler</a>
       <div class="col-sm-4">
           <button type="submit" class="btn btn-primary">Valider</button>
       </div>
   </div>
</form>

<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>