<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Création utilisateur"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">

	<h1> Nouvel utilisateur </h1>
</div>


<form method="POST" action="?page=user&act=<?= (isset($_GET['id']))? 'update&id='.$_GET['id'] : 'create' ?>">
    <div class="form-group row">
        <div class="col-sm-2 col-form-label">Civilité</div>
        <div class="col-sm-4">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="civ" id="civhomme" value="Homme" checked>
                <label class="form-check-label" for="civhomme">
                    Homme
                </label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="civ" id="civfemme" value="Femme">
                <label class="form-check-label" for="civfemme">
                    Femme
                </label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="nom" class="col-sm-2 col-form-label">Nom</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="nomutilisateur" name="nom" placeholder="Entrer le nom de l'utilisateur...">
        </div>
    </div>
    <div class="form-group row">
        <label for="prenom" class="col-sm-2 col-form-label">Prenom</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="prenomutilisateur" name="prenom" placeholder="entrer le prenom de l'utilisateur...">
        </div>
    </div>
    <div class="form-group row">
        <label for="mailPro" class="col-sm-2 col-form-label">Mail</label>
        <div class="col-sm-4">
            <input type="email" class="form-control" id="prenomutilisateur" name="mail" placeholder="entrer votre email">
        </div>
    </div>
    <div class="form-group row">
        <label for="mailPerso" class="col-sm-2 col-form-label">Mot de passe</label>
        <div class="col-sm-4">
            <input type="password" class="form-control" id="prenomutilisateur" name="mdp" placeholder="entrer votre mot de passe">
        </div>
    </div>
    <div class="form-group row">
        <a href="index.php" class="col-sm-2 col-form-label">Annuler</a>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-primary">Valider</button>
        </div>
    </div>
</form>



<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>