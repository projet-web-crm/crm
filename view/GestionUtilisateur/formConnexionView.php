<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Connexion"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">

	<h1> Connexion</h1>
</div>


<form>
    <div class="form-group row">
        <label for="admail" class="col-sm-2 col-form-label">Adresse mail</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="admail" placeholder="Entrez votre adresse mail...">
        </div>
    </div>
    <div class="form-group row">
        <label for="mdp" class="col-sm-2 col-form-label">Mot de passe</label>
        <div class="col-sm-4">
            <input type="password" class="form-control" id="mdp" placeholder="Entrez votre mot de passe...">
        </div>
    </div>
    <button type="button" class="btn btn-primary offset-5">Valider</button>
</form>



<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>