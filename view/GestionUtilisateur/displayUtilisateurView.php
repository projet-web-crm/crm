<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Infos utilisateur"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>


<div class="jumbotron">

  <h1>Infos utilisateur</h1>
</div>


<div class="container">
  <div class="row">
    <div class="col-12 list-group-item">
      
      <p> - <?= $utilisateur->getNom() ?> </p>
      <p> - <?= $utilisateur->getPrenom() ?> </p>
      <p> - <?= $utilisateur->getCivilite() ?> </p>
      <p> - <?= $utilisateur->getMailpro() ?> </p>
      <p> - <?= $utilisateur->getMailperso() ?> </p>


      
      <div class="">
      <button type="button" class="btn btn-primary">Modifier</button>
      <button type="button" class="btn btn-primary">Supprimer</button>
      </div>

    </div>
  </div>
</div>

<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>