<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 12/02/19
 * Time: 16:21
 */
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
    <meta content="text/html" charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="public/Bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?= $css ?>"/>
</head>
<body style="min-height: 100vh; width: 100vw; padding-bottom: 4rem;">

    <script src="public/Bootstrap/js/jquery-3.3.1.min.js"></script>
    <header>

            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

            <?php if (isset($_SESSION['connect'])) { ?>

              <a class="navbar-brand" href="?page=user&act=read">Accueil</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              
                <a class="navbar-brand" href="?page=client&act=all">Clients</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <a class="navbar-brand" href="?page=produit&act=all">Produits</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <a class="navbar-brand" href="?page=tva&act=all">TVA</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

            <?php } ?>
            </nav>
            
    </header>
    <div class="container col-11 justify-content-center">
    <?= $content ?>
    </div>
    <footer class="page-footer font-small blue fixed-bottom" style="height: 4rem; ">

          <!-- Copyright -->
          <div class="footer-copyright text-center py-3">© 2019 Copyright:
            <a href="?">NYC'</a>
          </div>
          <!-- Copyright -->

    </footer>

    <script src="public/Bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>