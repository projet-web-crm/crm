<?php
/**
 * Created by PhpStorm.
 * User: clement
 * Date: 10/02/19
 * Time: 21:48
 */

 $title = "Nouveau devis"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>

<div class="jumbotron">
  <h1>Nouveau devis</h1>

</div>

 <h3> <p> Nom client </p> </h3>

<div>



   <select class="" id="listetype">

       <option value="Particulier"> Particulier </option>
       <option value="Entreprise"> Entreprise</option>

  
   </select>

    <select class="" id="listeadressefacture">

        <option value="Adresse1"> Adresse 1 </option>
        <option value="Adresse2"> Adresse 2 </option>
        <option value="Adresse3"> Adresse 3 </option>
        <option value="Adresse4"> Adresse 4 </option>
    </select>

    <select class="" id="listeadresselivraison">

        <option value="Adresse1"> Adresse 1 </option>
        <option value="Adresse2"> Adresse 2 </option>
        <option value="Adresse3"> Adresse 3 </option>
        <option value="Adresse4"> Adresse 4 </option>
  
    </select>
</div>

<div class="container mt-5">
    <h4 class="breadcrumb-item"> Details devis</u> </h4>

    <div class="row mt-5">

        <button type="button" class="btn btn-primary btn-sm align-self-end">+</button>

	    <div class="col-sm-3">
            <p> Description de l'article </p>
            <input class="col-sm-12" type="text" placeholder="ART1"></input>
        </div>

        <div class="col-sm">
        </div>   

        <div class="col-sm-2">
            <p> Prix H.T </p>
            <input class="col-sm-6" type="text"></input>
        </div>

        <div class="col-sm">
        </div>  

        <div class="col-sm-1">
            <p> Qte </p>
            <input class="col-sm" type="text"></input>
        </div>

        <div class="col-sm">
        </div>  

        <div class="col-sm-2">
            <p> Remise </p>
            <input class="col-sm" type="text"></input>
        </div>

        <div class="col-sm">
        </div>  

        <div class="col-sm-2">
          <p> Prix TTC </p>
          <input class="col-sm-6" type="text"></input>
        </div>

            <div class="col-sm">
        </div> 

</div>

<div class="container col-12">

    <div class="row mt-5 offset-8">
        <p class="col-sm-7"> Sous-total HT </p>
        <p class="col-sm"> XXXX </p>
    </div>

    <div class="row offset-8">
    <p class="col-sm-7"> Remise </p>
    <p class="col-sm"> XXXX </p>
    </div>

    <div class="row offset-8">
    <p class="col-sm-7"> Livraison </p>
    <p class="col-sm"> XXXX </p>
    </div>

    <div class="row offset-8">
    <p class="col-sm-7"> Total TTC </p>
    <p class="col-sm"> XXXX </p>
    </div>

<button type="button" class="btn btn-primary btn-sm align-self-end offset-10 col-sm-1 mb-5">Valider</button>

</div>



<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>