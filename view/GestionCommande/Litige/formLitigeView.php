<?php

 $title = "Litige"; ?>

<?php $css = ""; ?>

<?php ob_start(); ?>

<div class="jumbotron"> 
    <h1>Litige</h1>
</div>

<form>
    <div class="form-group row">
            <label for="date" class="col-sm-2 col-form-label">Date du litige</label>
        <div class="col-sm-4">
            <input type="date" class="form-control" id="date" placeholder="Indiquer la date du litige">
        </div>
    </div>
    <div class="form-group row">
            <label for="commentaire" class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-4">
            <textarea class="form-control" rows="5" id="commentaire" placeholder="Décrire le motif du litige"></textarea>
        </div>
    </div>
    <div class="form-group row">
        <a class="col-sm-2 col-form-label" href="index.php">Annuler</a>
        <div class="col-sm-4">
        <button type="submit" class="btn btn-primary">Ajouter le litige</button>
        </div>
    </div>
</form>


<?php $content = ob_get_clean(); ?>

<?php require 'view/template.php'; ?>